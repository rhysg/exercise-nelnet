import { useState } from 'react';
import ReactSlider from 'react-slider';

const PaymentAmountInput = () => {
  const [paymentAmount, setPaymentAmount] = useState(1951);
  const [hasError, setHasError] = useState(false);
  const maxPayment = 20000;
  const handleInputChange = (e) => {
    // check error
    // if value > limit, or if not a number setHasError(true)
    const val = e.target.value;
    if (val > 200000) setHasError(true)
    // assume good for now
    setPaymentAmount(val)
  }
  const handleSliderChange = (value, index) => {
    console.log(value);
    setPaymentAmount(value)
  }
  return (
    <div className="">
      
      <div className="mx-auto border border-8 border-rose-500 rounded-full px-2 py-36 max-w-sm">
        
        <div><label>Payment Amount</label></div>
        <div className="border border-2 rounded-full border-lime-500 p-2">
        <input type="text" value={`$${paymentAmount}`} onChange={handleInputChange} />

        <div>
        <ReactSlider
            max={maxPayment}
            className="horizontal-slider"
            thumbClassName="bg-teal-300 px-2 py-1  border rounded-full border-2 border-black"
            trackClassName="bg-lime-500 h-2 top-4"
            onBeforeChange={(value, index) => console.log(`onBeforeChange: ${JSON.stringify({ value, index })}`)}
            onChange={(value, index) => handleSliderChange(value, index)}
            onAfterChange={(value, index) => console.log('after')}
            renderThumb={(props, state) => <div {...props}>{hasError ? "X" : "✓"}</div>}
        />
        </div>
        
        </div>
        
      </div>
      
    </div>
  )
}
export default PaymentAmountInput;
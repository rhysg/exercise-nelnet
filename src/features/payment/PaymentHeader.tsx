import CloseButton from '@components/CloseButton';
import SingleCell from '@components/SingleCell';

const PaymentHeader = () => {
  return (
    <div className="flex">
      <div>
        <CloseButton />
      </div>
      <div className="flex-auto text-right">
        <SingleCell name="Loan Balance" value="$44,008.41" />
        <SingleCell name="Loan" value="962-867-5309" />
      </div>
      
    </div>
  )
}
export default PaymentHeader;
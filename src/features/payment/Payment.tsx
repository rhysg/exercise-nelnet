import { useState } from 'react'
import PaymentHeader from './PaymentHeader';
import PaymentAmountInput from './PaymentAmountInput';
import PaymentTable from './PaymentTable';
import PaymentFooter from './PaymentFooter';

const Payment = () => {
  const [paymentData, setPaymentData] = useState();
  return (
    <div className="sm-container mx-auto border border-1 border-black rounded">
      <PaymentHeader />
      <PaymentAmountInput />
      <PaymentTable />
      <PaymentFooter />
    </div>
  )
}

export default Payment;
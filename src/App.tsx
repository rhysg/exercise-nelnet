
import './App.css'

import Payment from '@features/payment/Payment';

function App() {
  

  return (
    <div className="App container">
      <Payment />
    </div>
  )
}

export default App

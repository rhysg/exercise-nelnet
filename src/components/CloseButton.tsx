const CloseButton = () => {
  return (
    <button className="text-sm bg-teal-300 px-2 py-1  border rounded-full border-2 border-black">
      X
    </button>
  )
}
export default CloseButton;
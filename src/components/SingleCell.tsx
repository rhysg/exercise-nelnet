const SingleCell = ({name, value}) => {
  return (
    <>
      <div className="font-mono">
        {name}: {value}
      </div>
    </>
  )
}
export default SingleCell;